import {writable} from "svelte/store";

export const page = writable<string>(null);
// When adding new pages, don't forget to add them in ../App.svelte too!
// @ts-ignore
export const pages = new Set([
    'about',
    'display',
    'fis',
    'foo',
    'home'
])
const onHashChange = () => {
    const {hash} = location;
    const [, query] = hash.split('/');
    if (pages.has(query)) {
        page.set(query);
    } else {
        page.set('home');
        if (hash != '#/') {
            location.hash = '#/';
        }
    }
};
window.addEventListener('hashchange', onHashChange);
onHashChange();
